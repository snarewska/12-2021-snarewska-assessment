package com.snarewska.assessment.employee;

import com.snarewska.assessment.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepo employeeRepo;

    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public List<Employee> getEmployees() {
        return employeeRepo.getEmployees();
    }

    public void save(Employee employee) {
        employeeRepo.save(employee);
    }

    public void delete(Long id) {
        Employee employee = getById(id);
        employeeRepo.delete(employee);
    }

    public Employee getById(long id) {
        return employeeRepo.findById(id);
    }

    public void updateSalary(long id, Integer salary) {
        Employee employee = getById(id);
        employee.setSalary(salary);
        employeeRepo.save(employee);
    }
}
