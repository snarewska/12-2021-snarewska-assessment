package com.snarewska.assessment.employee;

import com.snarewska.assessment.entity.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class EmployeeRepo {

    @PersistenceContext
    private EntityManager entityManager;

    private static final String GET_EMPLOYEES = "select * FROM employee";

    @Transactional
    public List<Employee> getEmployees(){
        return (List<Employee>) entityManager.createNativeQuery(GET_EMPLOYEES, Employee.class).getResultList();
    }

    @Transactional
    public void save(Employee employee) {
        entityManager.persist(employee);
    }

    @Transactional
    public void delete(Employee employee) {
        entityManager.remove(employee);
    }

    @Transactional
    public Employee findById(long id) {
        return entityManager.find(Employee.class, id);
    }
}
