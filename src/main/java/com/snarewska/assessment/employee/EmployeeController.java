package com.snarewska.assessment.employee;

import com.snarewska.assessment.entity.Employee;
import jdk.dynalink.linker.LinkerServices;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/administration")
@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employee")
    public Employee getById(@RequestParam long id) {
        return employeeService.getById(id);
    }

    @GetMapping("/employees")
    public List<Employee> getAll() {
        return employeeService.getEmployees();
    }

    @PostMapping("/employee")
    public void save(@RequestBody Employee employee) {
        employeeService.save(employee);
    }

    @DeleteMapping("/employee")
    public void delete(@RequestParam Long id) {
        employeeService.delete(id);
    }

    @PutMapping
    public void updateSalary(@RequestParam long id, @RequestParam Integer salary) {
        employeeService.updateSalary(id, salary);
    }


}
