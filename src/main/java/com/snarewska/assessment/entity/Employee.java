package com.snarewska.assessment.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "Email")
    private String email;

    @Column(name = "phone_number")
    private Integer phoneNumber;

    @Column(name = "date_of_employment")
    private Date dateOfEmployment;

    @Column(name = "salary")
    private Integer salary;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToOne
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToOne
    @JoinColumn(name = "position_id")
    private Position position;

    public Employee(long id, String name, String surname, String email, Integer phoneNumber, Date dateOfEmployment,
                    Integer salary, Address address, Department department, Position position) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.dateOfEmployment = dateOfEmployment;
        this.salary = salary;
        this.address = address;
        this.department = department;
        this.position = position;
    }
}
